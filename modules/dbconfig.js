var dbconfig = {
    connectionLimit: 5, //Create a pool of 5 connection
    host: "10.10.6.159", //CloudSQL IP address - get IPV4
    user: "chuk",
    password: "chuk",
    database: "chuk_db"
};

//Attach object to module.exports so that it can be returned
//when loaded - viz. require()
module.exports = dbconfig;
