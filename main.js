//Load express library
var express = require("express");

//Create an application
var app = express();

//Load express-handlebars
var handlebars = require("express-handlebars");
//Ask our app to use handlebars as the template engine
//Default layout to use is main (main.handlbars)
app.engine("handlebars", 
    handlebars({
        defaultLayout: "main",
        helpers: {
            newRow: function(currId, newId) {
                if (currId == newId)
                    return ("success");
                return ("");
            },
            isActive: function(view, actual) {
                if (view == actual)
                    return ("active");
                return ("");
            },
            liIsActive: function(view, actual) {
                if (view == actual)
                    return ("<li class='active'>");
                return ("<li>");
                
            }
        }
    })
);
app.set("view engine", "handlebars");

var config = {
    connectionLimit: 5, //Create a pool of 5 connection
    host: "173.194.107.105", //CloudSQL IP address - get IPV4
    user: "fred",
    password: "25 heng mui keng",
    database: "chuk_db"
};

//Load my dbconfig
//var config = require("./modules/dbconfig");

//Initialize mysql
var mysql = require("mysql");
//Create a connection pool
var connPool = mysql.createPool(config);

//Load body-parser for handling
// application/x-www-form-urlencoded
var bodyParser = require("body-parser");

//Process incoming request
app.use(bodyParser.urlencoded({extended: false}));

//GET /
app.get("/", function(req, res) {
    res.render("index");
});

//Search customer
//GET /search?namequery=fred
app.get("/search", function(req, res) {
    console.info("namequery = %s", req.query.namequery);
    //Get a connection from the pool
    connPool.getConnection(function(err, conn) {
        //Check for error
        if (err) {
            res.render("error", { error: err});
            return;
        }
        //Construct our SQL
        var q = "select * from customers where name like '%"
                + req.query.namequery + "%'";
        var paramQuery = "select * from customers where name like ? ";
        console.info("query: %s", q);
        //Perform the query
        try {
            conn.query(paramQuery, // query
                ["%" + req.query.namequery + "%"], //actual parameters based on position
                function (err, rows) {
                    if (err) {
                        res.render("error", {error: err});
                        return;
                    }
                    res.render("customers", {customers: rows});
                });
        } catch (ex) {
            res.render("error", {error: ex});
        } finally {
            conn.release();
        }
    });
});

//GET /menu/index, customers, register, about
app.get("/menu/:category", function(req, res) {
    var cat = req.params.category;
    switch (cat) {
        case "index": //if ((cat == "index") || (cat == "about") || (cat == "register"))
        case "about":
        case "register":
            res.render(cat, {
               currentDate: (new Date()).toString()
            });
            break;

        case "customers":
            //Get a connection from the pool
            connPool.getConnection(function(err, conn) {
                //If there are errro, report it
                if (err) {
                    console.error("get connection error: %s", err);
                    res.render("/index");
                    return;
                }
                //Perform the query
                try {
                    conn.query("select * from customers", function (err, rows) {
                        if (err) {
                            console.error("query has error: %s", err);
                            res.render("/index");
                            return;
                        }
                        //Render the template with rows as the context
                        console.log("res newId = " + req.query.newId);
                        res.render("customers", { 
                            customers: rows,
                            newId: req.query.newId || false
                        });
                    });
                } catch (ex) {
                    res.render("error", {error: ex});
                } finally {
                    //IMPORTANT! Release the connection 
                    conn.release();
                }
            })
            break;
        
        default:
            res.redirect("/");
    }
});

//Handle POST for registrations
app.post("/register", function(req, res) {
    //Insert into your database
    connPool.getConnection(function(err, conn) {
        if (err) {
            res.render("error", { error: err});
            return;
        }
        try {
            conn.query("insert into customers values (?, ?, ?, ?, ?)",
                [req.body.id, req.body.name, req.body.email, req.body.phone, new Date()],
                function(err, result) {
                    if (err) {
                        res.render("error", {error: err});
                        return;
                    }
                    res.redirect("/menu/customers?newId=" +  req.body.id);
                }
            )
        } catch (ex) {
            res.render("error", { error: ex});
        } finally {
            conn.release();
        }
    });
});


/*
//GET /customers
app.get("/customers", function(req, res) {
    console.info(">>> %s", req.originalUrl);
    res.render("customers");
});

//GET /register
app.get("/register", function(req, res) {
    console.info(">>> %s", req.originalUrl);
    res.render("register");
});
*/

//Set the public/static directory
//Create a landing page in public dir
app.use(express.static(__dirname + "/public"));

//Catch all
app.use(function(req, res, next) {
    console.error("File not found: %s", req.originalUrl);
    res.redirect("/");
});

app.set("port", process.env.APP_PORT || 3000);

//Start the server
app.listen(app.get("port"), function() {
    console.info("Application started on port %s", app.get("port"));
});








